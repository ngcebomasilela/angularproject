import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TeamListComponent } from './team-list/team-list.component';
import { TeamCreateComponent } from './team-create/team-create.component';
import { TeamDeleteComponent } from './team-delete/team-delete.component';
import {TeamUpdateComponent} from './team-update/team-update.component';


import{PlayerListComponent} from './player-list/player-list.component'
import { PlayerCreateComponent } from './player-create/player-create.component';
import { PlayerDeleteComponent } from './player-delete/player-delete.component';
import { PlayerUpdateComponent } from './player-update/player-update.component';



const routes: Routes = [
  {path:'teams', component:TeamListComponent},
  {path:'create-team', component:TeamCreateComponent},
  {path:'update-team', component:TeamUpdateComponent},
  {path:'delete-team', component:TeamDeleteComponent},


  {path:'player',component:PlayerListComponent},
  {path:'player-create', component:PlayerCreateComponent},
  {path:'player-delete',component:PlayerDeleteComponent},
  {path:'player-update',component:PlayerUpdateComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
