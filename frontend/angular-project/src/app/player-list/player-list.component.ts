import { Component, OnInit } from '@angular/core';
import  {PlayerService} from '../services/player.service';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent implements OnInit {


   //teams :object;
  constructor(private data:PlayerService) { }

players =[
  {
    "PlayerID": 1,
    "Name": "Themba",
    "Surname": "Zwane",
    "Age": 1,
    "Tournament": null
}
]

 //players :object;


  ngOnInit(): void {
   this.data.getPlayers().subscribe(res=>{
    // this.players = res;
     console.log(res)
   });
  }

}
