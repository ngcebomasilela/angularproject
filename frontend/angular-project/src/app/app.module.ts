import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TeamCreateComponent } from './team-create/team-create.component';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamUpdateComponent } from './team-update/team-update.component';
import { TeamDeleteComponent } from './team-delete/team-delete.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PlayerCreateComponent } from './player-create/player-create.component';
import { PlayerListComponent } from './player-list/player-list.component';
import { PlayerUpdateComponent } from './player-update/player-update.component';
import { PlayerDeleteComponent } from './player-delete/player-delete.component';

@NgModule({
  declarations: [
    AppComponent,
    TeamCreateComponent,
    TeamListComponent,
    TeamUpdateComponent,
    TeamDeleteComponent,
    NavbarComponent,
    PlayerCreateComponent,
    PlayerListComponent,
    PlayerUpdateComponent,
    PlayerDeleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
