import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private http:HttpClient) { }

  getTeams(){
   return this.http.get("https://localhost:44333/api/Team/GetTeam");
  }
}
