﻿using MyAPI.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Web.Http.Cors;
namespace MyAPI.Controllers
{
    public class TeamController : ApiController
    {
        [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
        // GET: api/Team
        [System.Web.Http.Route("api/Team/GetTeam")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetTeam()
        {
            Football_LeagueDBEntities1 db = new Football_LeagueDBEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            return getTeamReturnList(db.Teams.ToList());
        }
        private List<dynamic> getTeamReturnList(List<Team> forClient)
        {
            List<dynamic> dynamicTeamTypes = new List<dynamic>();
            foreach (var team in forClient)
            {
                dynamic dynamicTeam = new ExpandoObject();
                dynamicTeam.TeamID = team.TeamID;
                dynamicTeam.Name = team.Name;
                dynamicTeam.Captain = team.Captain;
                dynamicTeam.TournamentID = team.TournamentID;
                dynamicTeamTypes.Add(team);
            }
            return dynamicTeamTypes;
        }
        [System.Web.Http.Route("api/Team/GetAllTeamWithPlayers")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetAllTeamWithPlayers()
        {
            Football_LeagueDBEntities1 db = new Football_LeagueDBEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            List<Team> teams = db.Teams.Include(zz => zz.Players).ToList();
            return GetAllTeamWithPlayers(teams);
        }
        private List<dynamic> GetAllTeamWithPlayers(List<Team> forClient)
        {
            List<dynamic> dynamicTeam = new List<dynamic>();
            foreach (Team teamss in forClient)
            {
                dynamic obForClient = new ExpandoObject();
                obForClient.TeamID = teamss.TeamID;
                obForClient.Name = teamss.Name; 
                obForClient.Captain = teamss.Captain;
                obForClient.TournamentID = teamss.TournamentID;
                obForClient.Player = getPlayer(teamss);
                dynamicTeam.Add(obForClient);
            }
            return dynamicTeam;
        }
        private List<dynamic> getPlayer(Team teamss)
        {
            List<dynamic> dynamicPlayer = new List<dynamic>();
            foreach (Player player1 in teamss.Players)
            {
                dynamic playr = new ExpandoObject();
                playr.PlayerID = player1.PlayerID;
                playr.Name = player1.Name;
                playr.surname = player1.Surname;
                playr.age = player1.Age;
                playr.TeamID = player1.TeamID;
                dynamicPlayer.Add(playr);
            }
            return dynamicPlayer;
        }
        // Teams
        [System.Web.Http.Route("api/Team/AddTeam")]
        [System.Web.Mvc.HttpPost]
        public List<Team> AddTeam([FromBody] Team team)
        {
            if (team != null)
            {
                Football_LeagueDBEntities1 db = new Football_LeagueDBEntities1();
                db.Configuration.ProxyCreationEnabled = false;

                var firstTournamentId = db.Tournaments.First(item=>item.TournamentID ==1);

                var newTeam = new Team
                {
                    Captain = team.Captain,
                    Name = team.Name,
                    TournamentID = firstTournamentId.TournamentID
                };

                db.Teams.Add(newTeam);
                db.SaveChanges();
                return db.Teams.ToList();
            }
            else return null;

        }
        //delete team
        [System.Web.Http.Route("api/Team/DeleteTeam/{id}")]
        [System.Web.Mvc.HttpDelete]

        public void DeleteTeam(int id)
        {
            if (id == null)
            {
                return;
            }
            else
            {
                Football_LeagueDBEntities1 db = new Football_LeagueDBEntities1();
                db.Configuration.ProxyCreationEnabled = false;
                Team team = db.Teams.Single(zz => zz.TeamID == id);
                db.Teams.Remove(team);
                db.SaveChanges();
            }

        }

        //update team 
        [System.Web.Http.Route("api/Team/UpdateTeam/{id}")]
        [System.Web.Mvc.HttpPut]
        public void UpdateDepartment(int id, [FromBody] Team team)
        {

            Football_LeagueDBEntities1 db = new Football_LeagueDBEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            Team teamInDb = db.Teams.Single(zz => zz.TeamID == id);

            //Update on name
            teamInDb.Name = team.Name;
            db.SaveChanges();
        }
    }
}

 

      
 
       

