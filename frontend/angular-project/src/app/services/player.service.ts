import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http:HttpClient) { }
  getPlayers(){
    return this.http.get("https://localhost:44333/api/Team/GetPlayers");
  }
}
