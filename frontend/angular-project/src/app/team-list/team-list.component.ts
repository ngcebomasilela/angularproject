import { Component, OnInit } from '@angular/core';
import  {TeamService} from '../services/team.service';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {

/*teams =[
  {

    "TeamID": 1,
    "Name": "Kaizer Chiefs 1",
    "Captain": "katsande",
    "TournamentID": 1,
    "Coach": null,
    "Players": [],
    "Tournament": null
},
{
  "TeamID": 2,
  "Name": " Sundowns",
  "Captain": "hlompho",
  "TournamentID": 1,
  "Coach": null,
  "Players": [],
  "Tournament": null
},
{
  "TeamID": 3,
  "Name": "Bidvest wits",
  "Captain": "Thulani",
  "TournamentID": 1,
  "Coach": null,
  "Players": [],
  "Tournament": null
}
]*/

 teams :object;

  constructor(private data:TeamService) { }


  ngOnInit(): void {
   this.data.getTeams().subscribe(res=>{
    this.teams = res;
     console.log(res)
   });
  }

}
