USE [master]
GO
/****** Object:  Database [Football LeagueDB]    Script Date: 2020/03/15 23:04:26 ******/
CREATE DATABASE [Football LeagueDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FootballLeagueDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\FootballLeagueDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'FootballLeagueDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\FootballLeagueDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Football LeagueDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Football LeagueDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Football LeagueDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Football LeagueDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Football LeagueDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Football LeagueDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Football LeagueDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [Football LeagueDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Football LeagueDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Football LeagueDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Football LeagueDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Football LeagueDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Football LeagueDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Football LeagueDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Football LeagueDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Football LeagueDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Football LeagueDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Football LeagueDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Football LeagueDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Football LeagueDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Football LeagueDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Football LeagueDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Football LeagueDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Football LeagueDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Football LeagueDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Football LeagueDB] SET  MULTI_USER 
GO
ALTER DATABASE [Football LeagueDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Football LeagueDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Football LeagueDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Football LeagueDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Football LeagueDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Football LeagueDB]
GO
/****** Object:  Table [dbo].[Coach]    Script Date: 2020/03/15 23:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coach](
	[CoachID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[Age] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Coach] PRIMARY KEY CLUSTERED 
(
	[CoachID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Player]    Script Date: 2020/03/15 23:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Player](
	[PlayerID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[Age] [int] NOT NULL,
	[TeamID] [int] NOT NULL,
 CONSTRAINT [PK_Player] PRIMARY KEY CLUSTERED 
(
	[PlayerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Prize]    Script Date: 2020/03/15 23:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Prize](
	[PrizeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Amount] [money] NOT NULL,
 CONSTRAINT [PK_Prize] PRIMARY KEY CLUSTERED 
(
	[PrizeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Team]    Script Date: 2020/03/15 23:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Team](
	[TeamID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Captain] [varchar](50) NOT NULL,
	[TournamentID] [int] NOT NULL,
 CONSTRAINT [PK_Team] PRIMARY KEY CLUSTERED 
(
	[TeamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tournament]    Script Date: 2020/03/15 23:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tournament](
	[TournamentID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Year] [date] NOT NULL,
 CONSTRAINT [PK_Tournament] PRIMARY KEY CLUSTERED 
(
	[TournamentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Coach] ON 

INSERT [dbo].[Coach] ([CoachID], [Name], [Surname], [Age]) VALUES (1, N'Steve', N'Komphela', N'52')
INSERT [dbo].[Coach] ([CoachID], [Name], [Surname], [Age]) VALUES (2, N'Gavin ', N'Hunt', N'55')
INSERT [dbo].[Coach] ([CoachID], [Name], [Surname], [Age]) VALUES (3, N'benni', N'McCarthy', N'42')
SET IDENTITY_INSERT [dbo].[Coach] OFF
INSERT [dbo].[Player] ([PlayerID], [Name], [Surname], [Age], [TeamID]) VALUES (0, N'mjbvj', N'ewrtfghjkl', 0, 1)
INSERT [dbo].[Player] ([PlayerID], [Name], [Surname], [Age], [TeamID]) VALUES (1, N'Themba ', N'zwane', 28, 1)
INSERT [dbo].[Player] ([PlayerID], [Name], [Surname], [Age], [TeamID]) VALUES (2, N'dennis ', N'onyango', 34, 2)
INSERT [dbo].[Player] ([PlayerID], [Name], [Surname], [Age], [TeamID]) VALUES (3, N'oupa', N'manyisa', 31, 3)
SET IDENTITY_INSERT [dbo].[Prize] ON 

INSERT [dbo].[Prize] ([PrizeID], [Name], [Amount]) VALUES (1, N'Cheque', 700000.0000)
INSERT [dbo].[Prize] ([PrizeID], [Name], [Amount]) VALUES (2, N'Cheque', 8000000.0000)
SET IDENTITY_INSERT [dbo].[Prize] OFF
SET IDENTITY_INSERT [dbo].[Team] ON 

INSERT [dbo].[Team] ([TeamID], [Name], [Captain], [TournamentID]) VALUES (1, N'Sundowns ', N'hlompho kekana', 1)
INSERT [dbo].[Team] ([TeamID], [Name], [Captain], [TournamentID]) VALUES (2, N'bidvest wits', N'thulani', 1)
INSERT [dbo].[Team] ([TeamID], [Name], [Captain], [TournamentID]) VALUES (3, N'baroka fc', N'phiri', 1)
INSERT [dbo].[Team] ([TeamID], [Name], [Captain], [TournamentID]) VALUES (11, N'Kaizer Chiefs edited', N'katsande edited', 1)
SET IDENTITY_INSERT [dbo].[Team] OFF
SET IDENTITY_INSERT [dbo].[Tournament] ON 

INSERT [dbo].[Tournament] ([TournamentID], [Name], [Year]) VALUES (1, N'NedbankCup', CAST(N'2017-05-05' AS Date))
INSERT [dbo].[Tournament] ([TournamentID], [Name], [Year]) VALUES (2, N'MTN 8 ', CAST(N'2019-07-07' AS Date))
INSERT [dbo].[Tournament] ([TournamentID], [Name], [Year]) VALUES (3, N'Black Label Cup ', CAST(N'2019-06-25' AS Date))
INSERT [dbo].[Tournament] ([TournamentID], [Name], [Year]) VALUES (4, N'CAF', CAST(N'2018-05-09' AS Date))
SET IDENTITY_INSERT [dbo].[Tournament] OFF
ALTER TABLE [dbo].[Coach]  WITH CHECK ADD  CONSTRAINT [FK_Coach_Team] FOREIGN KEY([CoachID])
REFERENCES [dbo].[Team] ([TeamID])
GO
ALTER TABLE [dbo].[Coach] CHECK CONSTRAINT [FK_Coach_Team]
GO
ALTER TABLE [dbo].[Player]  WITH CHECK ADD  CONSTRAINT [FK_Player_Team] FOREIGN KEY([TeamID])
REFERENCES [dbo].[Team] ([TeamID])
GO
ALTER TABLE [dbo].[Player] CHECK CONSTRAINT [FK_Player_Team]
GO
ALTER TABLE [dbo].[Prize]  WITH CHECK ADD  CONSTRAINT [FK_Prize_Tournament] FOREIGN KEY([PrizeID])
REFERENCES [dbo].[Tournament] ([TournamentID])
GO
ALTER TABLE [dbo].[Prize] CHECK CONSTRAINT [FK_Prize_Tournament]
GO
ALTER TABLE [dbo].[Team]  WITH CHECK ADD  CONSTRAINT [FK_Team_Tournament] FOREIGN KEY([TournamentID])
REFERENCES [dbo].[Tournament] ([TournamentID])
GO
ALTER TABLE [dbo].[Team] CHECK CONSTRAINT [FK_Team_Tournament]
GO
USE [master]
GO
ALTER DATABASE [Football LeagueDB] SET  READ_WRITE 
GO
